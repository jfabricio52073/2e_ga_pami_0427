import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  rotas = [
    {
      path: "/cad-autor",
      text: "Autores",
      icon: "person-circle-outline"
    },
    {
      path: "/cad-editora",
      text: "Editoras",
      icon: "bookmark-outline"
    },
    {
      path: "/cad-livro",
      text: "Livros",
      icon: "book-outline"
    }
  ]
  constructor() {}

}
